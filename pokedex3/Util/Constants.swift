//
//  Constants.swift
//  pokedex3
//
//  Created by Ruben Dias on 05/10/2017.
//  Copyright © 2017 Ruben Dias. All rights reserved.
//

import Foundation

let URL_BASE = "https://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()
